module Utilities

    let findNumberOfCongruentPairs (range: list<int>) (coinSet: list<int>): int =
        let rec getCombinations (primary: list<int>) (set: list<list<int>>): list<list<list<int>>> =
            match set with
            | [] -> List.empty
            | _ -> [primary; set.Head]::(set.Tail |> getCombinations primary)

        let mutable minimalSets: list<list<int>> = []

        let lastIndex = range.[range.Length - 1]

        let rec findWhileNotEmpty iteration =
            if iteration <= lastIndex
            then
                let minimal = 
                    MinimalFinder.findMinimalCoinSet iteration coinSet

                if minimal <> []
                then
                    minimalSets <- List.append minimalSets [minimal]
                    findWhileNotEmpty (iteration + 1)

        findWhileNotEmpty range.[0]

        if minimalSets.Length = range.Length 
        then
            minimalSets
            |> List.collect (fun (set: list<int>) -> getCombinations set minimalSets)
            |> List.filter (fun (pair: list<list<int>>) -> pair.[0] = pair.[1])
            |> List.length
        else -1        
            

    let calculateDistinctProbability (range: list<int>) (coinSet: list<int>) =

        let addEmptyPair (pairsCount) = pairsCount + 1

        let divide100By (pairsCount: int): float =
            let pairsCountFloat = pairsCount |> float
            100.0/pairsCountFloat

        let numberCongruentPairs =
            coinSet
            |> findNumberOfCongruentPairs range

        let probability: float =
            match numberCongruentPairs with
            | -1 -> 0.0
            | _ -> numberCongruentPairs |> addEmptyPair |> divide100By

        probability

    let generateCoinSets =
        [for i in 2..96 do
            for j in (i+1)..97 do
                for k in (j+1)..98 do
                    for l in (k+1)..99 do
                        yield [1;i;j;k;l]]
