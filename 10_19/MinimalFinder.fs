module MinimalFinder

/// Finds the minimal subset of coins required to
/// sum to the given target value from the given coin set
let findMinimalCoinSet (target: int) (coinSet: list<int>): list<int> =

        /// Gets all the possible lists that can sum
        /// to the given target valuue
        let rec getAllPossibleSetTails (target: int) (set: list<int>): list<list<int>> =

            /// gets the possible coins that could be used to reach the amount
            /// without going over
            let getNextChoices (target: int) (currSet: list<int>): list<int> =
                currSet
                |> List.filter (fun x -> target - x >= 0)

            /// recursion helper:
            /// given the current number (num),
            /// appends num to the set of coins required to make target - num
            let getPossibleTailsFor (num: int): list<list<int>> =
                    match target - num with
                    | 0 -> [[num]]
                    | _ ->
                        set
                        |> List.filter (fun x -> x <= num)
                        |> getAllPossibleSetTails (target - num)
                        |> List.map (fun (setTail: list<int>) -> num::setTail)

            let set =
                if set.Length > 1 && target >= set.[1]
                then set.Tail
                else set

            set
            |> getNextChoices target
            |> List.collect getPossibleTailsFor

        // sort the list of all valid sets by length
        let sortedMinimalSets =
            coinSet
            |> getAllPossibleSetTails target
            |> List.sortBy (fun (x: list<int>) -> x.Length)

        // get the length of the smallest set
        let minimalLength =
            sortedMinimalSets
            |> List.item 0
            |> List.length

        // check if there are any other equally small sets,
        // return the empty set if the coin set is not minimal,
        // return the minimal subset
        let minimalSet =
            sortedMinimalSets
            |> List.takeWhile (fun (x: List<int>) -> x.Length = minimalLength)

        if List.length minimalSet > 1
        then []
        else
            minimalSet
            |> List.item 0
            |> Seq.distinct
            |> Seq.sort
            |> Seq.toList