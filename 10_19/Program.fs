﻿// Learn more about F# at http://fsharp.org
open System

[<EntryPoint>]
let main argv =

    let stopwatch = Diagnostics.Stopwatch.StartNew()

    let mutable largestProbability = 0.0;
    let mutable largestSet = []
    let mutable count = 0;

    Utilities.generateCoinSets
    |> List.iter (fun set ->
        let probabilty = set |> Utilities.calculateDistinctProbability [1..99]
        if probabilty > largestProbability 
        then 
            largestProbability <- probabilty
            largestSet <- set
        count <- count + 1
        printfn "%f percent complete -- best set is %A with probability %f" (100.0 * ((count |> float) / 3_764_376.0)) largestSet largestProbability )

    stopwatch.Stop()

    printfn "Running time: %f minutes" ((stopwatch.ElapsedMilliseconds |> float) / (1000.0 * 60.0))

    0 // return an integer exit code